from django.db import models

# Create your models here.

from django.contrib.auth.models import AbstractUser
import datetime
from tinymce.models import HTMLField

status = (
    ('TO DO', 'TO DO'),
    ('IN PROGRESS', 'IN PROGRESS'),
    ('IN REVIEW', 'IN REVIEW'),
    ('DONE', 'DONE'),
)
type = (('Story', 'Story'),
        ('Task', 'Task'),
        ('Bug', 'Bug'),
        ('Defect', 'Defect')
        )
activate = ((0, 'Pending'),
            (1, 'Active'),
            (2, 'Deactivate'),
            (3, 'Delete'))

user_role = ((0, 'Developer'),
             (1, 'Manager'),
             (2, 'Admin'))


class User(AbstractUser):
    activate = models.IntegerField(choices=activate, default=1)
    user_role = models.IntegerField(choices=user_role, default=2)
    updated_by = models.CharField(max_length=100,null=True,)
    def __str__(self):
        return self.username


class Project(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = HTMLField()
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    start_date = models.DateField(default=datetime.date.today)
    end_date = models.DateField(null=True)
    is_active = models.BooleanField(default=True)


    def __str__(self):
        return self.name

    def get_tasks(self):
        """
        With the help of this function we can get all tasks belonging to this project

        """
        return Task.objects.filter(project=self)


class Sprint(models.Model):
    name = models.CharField(max_length=50, unique=True, db_index=True)
    project = models.ForeignKey(Project, related_name='sprints', on_delete=models.CASCADE, null=True)
    start_date = models.DateField(default=datetime.date.today)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL,null=True)
    is_active = models.BooleanField(default=True)
    end_date = models.DateField(null=True)

    def __str__(self):
        return self.name


class Task(models.Model):
    priority = (("Low", "Low"),
                ("Medium", "Medium"),
                ("High", "High"))
    title = models.CharField(max_length=100, unique=True)
    task_type = models.CharField(max_length=15, choices=type, default='Task')
    status = models.CharField(max_length=15, choices=status, default='TO DO')
    priority = models.CharField(max_length=20, choices=priority, default='Major')
    updated_by = models.ForeignKey(User, related_name='task_updated_by', on_delete=models.SET_NULL, null=True)
    description = HTMLField()
    project = models.ForeignKey(Project, related_name='tasks', on_delete=models.CASCADE, null=True)
    start_date = models.DateField(default=datetime.date.today)
    end_date = models.DateField(null=True)
    assignee = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    expected_hours_to_finish = models.PositiveIntegerField(default=0)
    sprint = models.ForeignKey(Sprint, related_name='tasks', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.title

    def get_comments(self):
        """
        With the help of this function we can get all comments belonging to this task

        """
        return Comment.objects.filter(task=self)


class Comment(models.Model):
    description = HTMLField()
    task = models.ForeignKey(Task, related_name='comments', on_delete=models.CASCADE, null=True)
    write_date = models.DateField(default=datetime.date.today)
    worker = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.description
