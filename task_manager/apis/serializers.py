from rest_framework import serializers
from .models import Project, Task, Comment, Sprint
from .models import User


class UserSerializer(serializers.ModelSerializer):
    updated_by = serializers.CharField(max_length=150,required=False)
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'email', 'activate','user_role','updated_by']


class TaskSerializer(serializers.ModelSerializer):
    # comments = CommentSerializer(many=True, required=False)
    updated_by = serializers.SlugRelatedField(slug_field='username', read_only=True)

    class Meta:
        model = Task
        fields = '__all__'


class SprintSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sprint
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):
    owner = serializers.SlugRelatedField(slug_field='username',read_only=True)
    # sprints = SprintSerializer(many=True, read_only=True)

    class Meta:
        model = Project
        fields = '__all__'


# class ProjectRelatedSerializer(serializers.ModelSerializer):
#     tasks = TaskSerializer(read_only=True, many=True)
#
#     class Meta:
#         model = Project
#         fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):
    worker = serializers.SlugRelatedField(slug_field='username', read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'


class TaskDetailSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True, required=False)

    class Meta:
        model = Task
        fields = '__all__'


class SprintDetailSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, read_only=True)

    class Meta:
        model = Sprint
        fields = '__all__'
