from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('login', views.login, name='login'),
    path('register', views.register, name='register'),
    path('logout', views.logout, name='logout'),
    path('users/activate', views.activate_users, name='activate'),
    path('create_project', views.create_project, name='create_project'),
    path('create_task', views.create_task, name='create_task'),
    path('create_sprint', views.create_sprint, name='create_sprint'),
    path('create_comment', views.create_comment, name='create_comment'),
    path('project_list', views.project_list, name='project_list'),
    path('project_lists', views.project_lists, name='project_lists'),
    path('comment_list', views.comment_list, name='comment_list'),
    path('dashboard_details', views.dashboard_details, name='dashboard_details'),
    path('task_info', views.task_info, name='task_info'),
    path('project_progress', views.project_progress, name='project_progress'),
    path('project_info', views.project_info, name='project_info'),
    path('projects/<int:pk>/', views.project_detail),
    path('projects/<int:project_id>/task/<int:task_id>/', views.task_detail),
    path('projects/<int:project_id>/task/<int:task_id>/comments/<int:comment_id>', views.comment_detail),
    path('sprints/', views.sprint_search),
    path('projects/<int:project_id>/sprints/', views.sprint_list),
    path('projects/<int:project_id>/sprints/<int:sprint_id>/', views.sprint_info),
    path('tasks', views.task_search),
    path('users/<int:pk>', views.user_detail),
    path('users',views.user_search)
]
