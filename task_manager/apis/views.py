import json

from .models import User
from django.contrib.auth.models import auth, update_last_login
from django.http import Http404
from django.http import HttpResponse, JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import permission_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from functools import reduce
import operator
from django.db.models import Q

from .models import Project, Comment, Task, Sprint
from .permissions import IsSuperUser, IsManager
from .serializers import ProjectSerializer, CommentSerializer, SprintSerializer, TaskSerializer, UserSerializer, \
    SprintDetailSerializer, TaskDetailSerializer

from rest_framework.pagination import PageNumberPagination
from rest_framework.exceptions import PermissionDenied, AuthenticationFailed

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


def search(*list, platform):
    for i in range(len(list)):
        if list[i] == platform:
            return True
    return False


# Create your views here.

@api_view(['Post'])
@permission_classes([AllowAny])
def login(request):
    if request.method == "POST":
        serializer = request.body
        serial = json.loads(serializer)
        username = serial['username']
        password = serial['password']
        x = User.objects.get(username=username)

        if x and x.activate == 0:
            raise PermissionDenied(detail="Permission Denied. contact admin")

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            payload = JWT_PAYLOAD_HANDLER(user)
            jwt_token = JWT_ENCODE_HANDLER(payload)
            auth.login(request, user)
            update_last_login(None, user)
            resp = {"userid": user.id, 'username': user.username, 'token': jwt_token, 'user_level': user.is_superuser,
                    'user_role': user.user_role}
            return Response(resp, status.HTTP_202_ACCEPTED)
        else:
            raise AuthenticationFailed("Please provide valid username and password.", status.HTTP_401_UNAUTHORIZED)


@api_view(['Post'])
@permission_classes([AllowAny])
def register(request):
    if request.method == "POST":
        serializer = request.body
        serial = json.loads(serializer)
        username = serial['username']
        password = serial['password']
        email = serial['email']
        first_name = serial['first_name']
        last_name = serial['last_name']

        if User.objects.filter(username=username).exists():
            return JsonResponse({"detail": "Username already exists"},
                                status=status.HTTP_404_NOT_FOUND)
        elif User.objects.filter(email=email).exists():
            return JsonResponse({"detail": "Email already exists"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            user = User.objects.create_user(username=username, password=password, email=email, first_name=first_name,
                                            last_name=last_name, is_active=False, activate=0, user_role=0)
            user.save()
            return Response(status.HTTP_201_CREATED)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def logout(request):
    if request.method == "GET":
        auth.logout(request)
        return Response(status.HTTP_200_OK)


@api_view(['GET', "PUT"])
@permission_classes([IsSuperUser])
def activate_users(request):
    if request.method == "GET":
        paginator = PageNumberPagination()
        paginator.page_size = 10
        paginator.page_size_query_param = 'per_page'

        search_key = request.query_params.get('search_key', None)
        users = User.objects.exclude(activate=3)

        def set_if_not_none(mapping, key, value):
            if value:
                mapping[key] = value

        filter_q = {}
        set_if_not_none(filter_q, 'username__istartswith', search_key)
        set_if_not_none(filter_q, 'first_name__istartswith', search_key)
        set_if_not_none(filter_q, 'last_name__istartswith', search_key)
        set_if_not_none(filter_q, 'email__istartswith', search_key)

        if len(filter_q.keys()) != 0:
            users = users.filter(reduce(operator.or_, (Q(**d) for d in [dict([i]) for i in filter_q.items()])))

        result_page = paginator.paginate_queryset(users, request)
        serializer = UserSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    elif request.method == 'PUT':
        users = request.data
        for user in users:
            is_active = False
            if user['activate'] == 1:
                is_active = True
            User.objects.filter(id=user.get('id')).update(is_active=is_active, activate=user['activate'],
                                                          updated_by=request.user.username)
        users = User.objects.exclude(activate=3)
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsSuperUser])
def create_project(request):
    if request.method == "POST":
        serializer = ProjectSerializer(data=request.data, many=True)
        owner_id = request.data[0].get("owner")
        if serializer.is_valid():
            serializer.save(owner_id=owner_id)
            return Response(status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_task(request):
    if request.method == "POST":
        # project_id = request.data['project']
        # assignee_id = request.data['assignee']
        # sprint_id = request.data['sprint']
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(updated_by=request.user)
            return Response(status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsSuperUser | IsManager])
def create_sprint(request):
    if request.method == "POST":
        serializer = SprintSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_comment(request):
    if request.method == "POST":
        serializer = CommentSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save(worker=request.user)
            return Response(status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def project_list(request):
    if request.method == "GET":
        paginator = PageNumberPagination()
        paginator.page_size = 10
        paginator.page_size_query_param = 'per_page'

        def set_if_not_none(mapping, key, value):
            if value:
                mapping[key] = value

        search_key = request.query_params.get('name', None)
        user = request.query_params.get('user', None)
        # filter_q = {}
        # set_if_not_none(filter_q, 'owner__username__istartswith', user)
        # set_if_not_none(filter_q, 'name__istartswith', name)

        filter_q = {}
        set_if_not_none(filter_q, 'name__istartswith', search_key)
        set_if_not_none(filter_q, 'description__istartswith', search_key)
        set_if_not_none(filter_q, 'owner__username__istartswith', search_key)
        # set_if_not_none(filter_q, 'email__istartswith', search_key)

        if len(filter_q.keys()) != 0:
            projects = Project.objects.filter(
                reduce(operator.or_, (Q(**d) for d in [dict([i]) for i in filter_q.items()])))
        else:
            projects = Project.objects.all()

        result_page = paginator.paginate_queryset(projects, request)
        serializer = ProjectSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def project_lists(request):
    if request.method == "GET":
        name = request.query_params.get('name', None)
        if name is not None:
            projects = Project.objects.filter(name__istartswith=name)
        else:
            projects = Project.objects.all()
        serializer = ProjectSerializer(projects, many=True)
        return Response(serializer.data, status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def task_list(request):
    if request.method == "GET":
        tasks = Task.objects.filter(assignee=request.user)
        serializer = TaskSerializer(tasks, many=True)
        return Response(serializer.data, status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def comment_list(request):
    if request.method == "POST":
        serial = request.body
        serial1 = json.loads(serial)
        task_id = serial1['taskid']
        comment_count = Comment.objects.filter(task=task_id).count()
        if comment_count > 0:
            comments = Comment.objects.filter(task=task_id)
            serializer = CommentSerializer(comments, many=True)
            return Response(serializer.data, status.HTTP_200_OK)
        else:
            return Response(status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def sprint_search(request):
    if request.method == "GET":
        paginator = PageNumberPagination()
        paginator.page_size = 10
        paginator.page_size_query_param = 'per_page'

        def set_if_not_none(mapping, key, value):
            if value:
                mapping[key] = value

        name = request.query_params.get('name', None)
        project_name = request.query_params.get('projectname', None)
        filter_q = {}
        set_if_not_none(filter_q, 'name__istartswith', name)
        set_if_not_none(filter_q, 'project__name__istartswith', project_name)

        if len(filter_q.keys()) == 0:
            sprints = Sprint.objects.all()
        else:
            sprints = Sprint.objects.filter(**filter_q)
        result_page = paginator.paginate_queryset(sprints, request)
        serializer = SprintSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def sprint_list(request, project_id):
    if request.method == "GET":
        sprints = Sprint.objects.filter(project=project_id)
        serializer = SprintSerializer(sprints, many=True)
        return Response(serializer.data, status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def dashboard_details(request):
    if request.method == "GET":
        project_count = Project.objects.all().count()
        task_count = Task.objects.all().count()
        pending_projects = Project.objects.filter(is_active=True).count()
        pending_tasks = Task.objects.filter(status="DONE").count()

        resp = {}
        resp['project_count'] = project_count
        resp['task_count'] = task_count
        resp['pending_projects'] = pending_projects
        resp['pending_tasks'] = pending_tasks

        return Response(resp, status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def task_info(request):
    if request.method == "POST":
        serial = request.body
        serial1 = json.loads(serial)
        task_id = serial1['taskid']
        task_count = Task.objects.filter(id=task_id).count()
        if task_count > 0:
            task = Task.objects.filter(id=task_id)
            desc = {}
            desc['Title'] = task.title
            desc['Description'] = task.description
            user = User.objects.filter(id=task.worker)
            project = Project.objects.filter(id=task.project)
            desc['start_date'] = task.start_date
            desc['end_date'] = task.end_date
            desc['status'] = task.completed
            desc['worker'] = user.username
            desc['project'] = project.title
            desc['expected_hours'] = task.expected_hours_to_finish
            return Response(desc, status.HTTP_200_OK)
        else:
            return Response(status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
def sprint_info(request, project_id, sprint_id):
    sprint = Sprint.objects.get(id=sprint_id)
    if request.method == "GET":
        serializer = SprintSerializer(sprint)
        return Response(serializer.data, status.HTTP_200_OK)
    elif request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = SprintSerializer(sprint, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)
    elif request.method == 'DELETE':
        sprint.delete()
        return HttpResponse(status=204)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def task_search(request):
    if request.method == "GET":
        paginator = PageNumberPagination()
        paginator.page_size = 10

        paginator.page_size_query_param = 'per_page'

        def set_if_not_none(mapping, key, value):
            if value:
                mapping[key] = value

        priority = request.query_params.get('priority')
        title = request.query_params.get('title')
        project_name = request.query_params.get('projectname')
        sprint_id = request.query_params.get('sprintid')
        task_type = request.query_params.get('tasktype')
        task_status = request.query_params.get('taskstatus')
        filter_q = {}
        set_if_not_none(filter_q, 'title__istartswith', title)
        set_if_not_none(filter_q, 'priority', priority)
        set_if_not_none(filter_q, 'project__name__istartswith', project_name)
        set_if_not_none(filter_q, 'sprint_id', sprint_id)
        set_if_not_none(filter_q, 'task_type', task_type)
        set_if_not_none(filter_q, 'status', task_status)
        if len(filter_q.keys()) == 0:
            task = Task.objects.filter()
        else:
            task = Task.objects.filter(**filter_q)
        result_page = paginator.paginate_queryset(task, request)
        serializer = TaskSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)
        # return Response(task)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def project_progress(request):
    try:
        if request.content_type == 'application/json' or request.content_type == 'application/x-www-form-urlencoded':
            cat = request.body
            temp = json.loads(cat)
            project_id = temp['projectid']
            project_id = request.POST.get('project_id', False)
            is_active_tasks = Task.objects.filter(project=project_id, is_active=True).count()
            Total_tasks = Task.objects.filter(project=project_id).count()
        else:
            return Response({"Error": "content_type is not in json format"}, status=status.HTTP_400_BAD_REQUEST)

    except Task.DoesNotExist:
        raise Http404("Product does not exist")
    if request.method == 'POST':
        progress = (is_active_tasks / Total_tasks) * 100
        return Response({"Progress": progress}, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def project_info(request):
    try:
        if request.content_type == 'application/json' or request.content_type == 'application/x-www-form-urlencoded':
            cat = request.body
            temp = json.loads(cat)
            project_id = temp['project_id']
            # project_id = request.POST.get('project_id', False)
            project = Project.objects.filter(id=project_id)
        else:
            return Response({"Error": "content_type is not in json format"}, status=status.HTTP_400_BAD_REQUEST)

    except Project.DoesNotExist:
        raise Http404("Product does not exist")
    if request.method == 'POST':
        desc = {}
        user = User.objects.filter(id=project.owner)
        tasks = Task.objects.filter(project=project.id)
        workers = []
        for task in tasks:
            user = User.objects.filter(task.worker_id)
            workers.append(user.name)

        desc['Title'] = project.title
        desc['Description'] = project.description
        desc['Owner'] = user.username  # please use the field corresponding to User
        desc['start_date'] = project.start_date
        desc['end_date'] = project.end_date
        desc['is_active'] = project.is_active
        desc['workers'] = workers
        return Response(desc, status=status.HTTP_201_CREATED)


@api_view(['DELETE', 'GET', 'PUT'])
@permission_classes([IsAuthenticated])
def project_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        project = Project.objects.get(pk=pk)
    except Project.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'DELETE':
        project.delete()
        return HttpResponse("Project deleted", status=204)
    elif request.method == "GET":
        serializer = ProjectSerializer(project)
        return JsonResponse(serializer.data)
    # update project
    elif request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = ProjectSerializer(project, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)


@api_view(['DELETE', 'GET', 'PUT', 'PATCH'])
@permission_classes([IsAuthenticated])
def task_detail(request, project_id, task_id):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        related_task = Project.objects.get(pk=project_id).tasks.get(pk=task_id)  # for example
    except Task.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'DELETE':
        related_task.delete()
        return HttpResponse(status=204)
    elif request.method == "GET":
        serializer = TaskDetailSerializer(related_task)
        try:
            sprint_name = Sprint.objects.get(id=serializer.data['sprint']).name
        except Sprint.DoesNotExist:
            sprint_name = None
        new_response = {'sprint_name': sprint_name}
        new_response.update(serializer.data)

        return JsonResponse(new_response)
    # update task
    elif request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = TaskDetailSerializer(related_task, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)
    elif request.method == "PATCH":
        data = JSONParser().parse(request)
        serializer = TaskDetailSerializer(related_task, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)


@api_view(['DELETE', 'GET'])
@permission_classes([IsAuthenticated])
def comment_detail(request, project_id, task_id, comment_id):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        related_comment = Project.objects.get(pk=project_id).tasks.get(pk=task_id).comments.get(pk=comment_id)  #
        # for example
    except Comment.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'DELETE':
        related_comment.delete()
        return HttpResponse(status=204)
    elif request.method == "GET":
        serializer = CommentSerializer(related_comment)
        return JsonResponse(serializer.data)


@api_view(['PUT', 'GET'])
@permission_classes([IsAuthenticated])
def user_detail(request, pk):
    if request.method == "GET":
        user = User.objects.get(pk=pk)
        serializer = UserSerializer(user)
        return JsonResponse(serializer.data)
    if request.method == "PUT":
        try:
            user = User.objects.get(pk=pk)  # for example
        except Task.DoesNotExist:
            return HttpResponse(status=404)
        data = JSONParser().parse(request)
        serializer = UserSerializer(user, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def user_search(request):
    if request.method == "GET":
        name = request.query_params.get('name', None)
        if name is not None:
            users = User.objects.filter(activate=1).filter(username__istartswith=name)
        else:
            users = User.objects.filter(activate=1)

        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)
