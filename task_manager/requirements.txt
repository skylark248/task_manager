Django==3.1.2
django-cors-headers==3.5.0
django-tinymce==3.1.0
djangorestframework==3.12.1
djangorestframework-jwt==1.11.0
psycopg2==2.8.6
